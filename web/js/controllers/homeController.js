app.controller('homeController', function($scope, $http) {
  
  $scope.facturas = [];

  $scope.formatDate = function(dateIso) {
    var date = new Date(dateIso);
    return date.toLocaleDateString();
  };

  $scope.delete = function(factura) {
    var confirmation = confirm("Seguro que quiere eliminar la factura nro: " + factura.nroFactura );
    if (confirmation) {
      $http.delete('/facturas/' + factura.nroFactura).then(function(response) {
        // if any was removed
        if (response.data.n) {
          // refresh facturas
          getFacturasFromServer();
        } else {
          showError();
        }
      }, showError);
    }
  };

  var getFacturasFromServer = function() {
    $http.get('/facturas').then(function(response) {
      $scope.facturas = response.data;
    });
  };

  var showError = function() {
    alert('Ocurrió un error');
  };

  getFacturasFromServer();

});