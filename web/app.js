var app = angular.module('app', ['ngRoute']);

app.config(function($routeProvider, $locationProvider) {
  $locationProvider.hashPrefix('');

  $routeProvider
    .when('/', {
      controller:'homeController',
      templateUrl:'views/home.html'
    })
    .when('/factura/:nroFactura', {
      controller:'facturaController',
      templateUrl:'views/factura.html'
    })
    .when('/factura', {
      controller:'facturaController',
      templateUrl:'views/factura.html'
    })
    .otherwise({
      redirectTo:'/'
    });
});