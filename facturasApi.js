// Dependencies
var express = require('express');

var router = express.Router();

var parseNum = function(num) {
  return parseInt(num, 10);
}

var api = function(database) {
  
  var collection = database.get('facturas');
  
  router.get('/', function(req, res) {
    collection.find({}, {}, function(e, docs) {
      res.json(docs);
    });
  });

  router.get('/:nro', function(req, res) {
    var nroFactura = parseNum(req.params.nro);
    collection.findOne({ 'nroFactura' : nroFactura }, {}, function(e, doc) {
      res.json(doc);
    });
  });

  router.put('/:nro', function(req, res) {
    var nroFactura = parseNum(req.params.nro);
    collection.update({ 'nroFactura': nroFactura }, req.body, {}, function(e, doc) {
      res.json(doc);
    });
  });

  router.post('/', function(req, res) {
    collection.insert(req.body, {}, function(e, doc) {
      res.json(doc);
    });
  });

  router.delete('/:nro', function(req, res) {
    var nroFactura = parseNum(req.params.nro);
    collection.remove({ 'nroFactura': nroFactura }, {}, function(e, doc) {
      res.json(doc);
    });
  });

  return router;
}


module.exports = api;