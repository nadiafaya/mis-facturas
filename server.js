// Dependencies
var express = require('express')
var bodyParser = require('body-parser')
var monk = require('monk')
var facturasApi = require('./facturasApi');

// Constants
const PORT = 8000
const CONNECTION_URL = 'localhost:28000/test';

var app = express()
var database = monk(CONNECTION_URL);

// For parsing application/json
app.use(bodyParser.json());

// Serve all under web
app.use('/', express.static('web'))

// Serve api
app.use('/facturas', facturasApi(database));

// Start server
app.listen(PORT, function () {
  console.log('Server started on port', PORT)
})