app.controller('facturaController', function($scope, $routeParams, $http) {
  
  $scope.factura = {};

  $scope.editing = !!$routeParams.nroFactura;

  if ($scope.editing) {
    $http.get('/facturas/' + $routeParams.nroFactura).then(function(response) {
      $scope.factura = response.data;

      $scope.factura.fechaEmision = new Date($scope.factura.fechaEmision);
      $scope.factura.fechaVencimiento = new Date($scope.factura.fechaVencimiento);
    });
  }

  $scope.save = function() {
    var savePromise;
    var factura = angular.copy($scope.factura);
    if (factura.nroFactura) {
      factura.fechaEmision = factura.fechaEmision ? factura.fechaEmision.toISOString() : factura.fechaEmision;
      factura.fechaVencimiento = factura.fechaVencimiento ? factura.fechaVencimiento.toISOString() : factura.fechaVencimiento;
      if ($scope.editing) {
        savePromise = $http.put('/facturas/' + factura.nroFactura, factura);
      } else {
        savePromise = $http.post('/facturas/', factura);
      }
      savePromise.then(function(response) {
        // if modified or inserted
        if (response.data.nModified || response.data.nroFactura) {
          alert("Success!");
        } else {
          alert("Not updated");
        }
      }, function(error) {
        alert("Error! \n", error);
      });
    }
  };

});