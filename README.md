# MisFacturas.com
Este proyecto es una demo sencilla del stack de MEAN (Mongo, Express, Angular, Node) aplicado al ejemplo de Facturas visto en la clase de Mongo de la materia Implementación de Bases de Datos NoSQL de la UTN.

Además del stack de tecnologías mencionado se utilizan las siguientes librerías:

- Monk: [(link)](https://automattic.github.io/monk/) Es una librería para acceder a una base de datos Mongo desde Nodejs
- Bootstrap: [(link)](http://getbootstrap.com/) Es una librería para facilitar el maquetado de html y css

## Pre-requisitos:

Instalar:

* NodeJs [(link)](https://nodejs.org/en/download/)

* Git [(link)](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

* Mongo [(link)](https://www.mongodb.com/download-center)

## Instalación:

1) Clonar el proyecto con el siguiente comando:

`git clone https://bitbucket.org/nadiafaya/mis-facturas.git`

2) Entrar al directorio del proyecto:

`cd mis-facturas`

3) Instalar una por una las dependencias: 

`npm install express`

`npm install body-parser`

`npm install monk`

4) Iniciar el servidor de mongo:

`mongod --dbpath [path-a-directorio-de-data] --port 28000`

5) Iniciar el server de node:

`node server.js`

6) Navegar a http://localhost:8000

7) Profit!